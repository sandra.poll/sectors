##Installation
Install composer packages

`composer install`

`cp .env.example .env`

`php artisan key:generate`

Create database structure

`php artisan migrate`

Fill sectors table

`php artisan db:seed`

Install npm packages

`npm install`

`npm run production`

`php artisan serve`

##Versions

MariaDB 10.3.25
