require('./bootstrap');
import axios from 'axios';

const form = document.getElementById('sectorForm');
form.addEventListener('submit', submit);

function removeValidationMessages() {
    document.getElementById("validationMessages").innerHTML = "";
}

function updateUser(name, selectedSectors) {
    let userId = document.getElementById("userId").value;
    let url = '/api/users/' + userId;

    axios.put(url, {
        name: name,
        sectors: selectedSectors
    })
        .then(function (response) {
            document.getElementById("validationMessages").innerHTML = "Your data has been submitted!";

            setTimeout(function () {
                removeValidationMessages();
            }, 3000);
        })
        .catch(function (error) {
            let i, messages = "";
            let errorMessages = error.response.data.errors;

            for (i in errorMessages) {
                messages += errorMessages[i] + "<br>";
            }

            removeValidationMessages();
            document.getElementById("validationMessages").innerHTML += messages + "<br>";
        });
}

function saveUser(name, selectedSectors) {
    axios.post('/api/users', {
        name: name,
        sectors: selectedSectors
    })
        .then(function (response) {
            let id = response.data.id;
            window.location.replace("http://127.0.0.1:8000/" + id);
            document.getElementById("validationMessages").innerHTML = "Your data has been submitted!";

            setTimeout(function () {
                removeValidationMessages();
            }, 3000);
        })
        .catch(function (error) {
            let i, messages = "";
            let errorMessages = error.response.data.errors;

            for (i in errorMessages) {
                messages += errorMessages[i] + "<br>";
            }
            removeValidationMessages();
            document.getElementById("validationMessages").innerHTML += messages;
        });
}

function submit() {

    let name = document.getElementById("name").value;
    let sectors = document.getElementById("sectors");
    let terms = document.getElementById("terms").checked;
    let collection = sectors.selectedOptions;
    let selectedSectors = [];

    if (terms === false) {
        event.preventDefault();
        removeValidationMessages();
        document.getElementById("validationMessages").innerHTML += "Agree to terms checkbox is required!";
    } else {

        for (let i = 0; i < collection.length; i++) {
            selectedSectors.push(collection[i].id);
        }

        event.preventDefault();

        if (document.getElementById("userId") != null) {
            updateUser(name, selectedSectors);
        } else {
            saveUser(name, selectedSectors);
        }
    }
}
