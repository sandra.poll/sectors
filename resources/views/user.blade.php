<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sectors</title>

    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}"/>
</head>
<body class="antialiased">

<h3>Please enter your name and pick the Sectors you are currently involved in.</h3>

<br>

<form id="sectorForm">
    @csrf
    <label for="name">Name</label>
    <input name="name" id="name" type="text" @isset($user) value="{{$user->name}}" @endif>

    <br>
    <br>

    <label for="sectors">Sectors</label>
    <select name="sectors" id="sectors" multiple size="15">
        @foreach ($sectors as $sector)
            @if($sector->is_group_name === 1)
                <optgroup label="{{$sector->name}}" class="level-{{$sector->level}}"></optgroup>
            @else
                @isset($user)
                    @if(in_array($sector->id, $user->sector_ids))
                        <option value="{{$sector->id}}" id="{{$sector->id}}"
                                class="level-{{$sector->level}}" selected>{{$sector->name}}</option>
                    @else
                        <option value="{{$sector->id}}" id="{{$sector->id}}"
                                class="level-{{$sector->level}}">{{$sector->name}}</option>
                    @endif
                @else
                    <option value="{{$sector->id}}" id="{{$sector->id}}"
                            class="level-{{$sector->level}}">{{$sector->name}}</option>
                @endif
            @endif
        @endforeach
    </select>

    <br>
    <br>

    <label for="terms">Agree to terms</label>
    <input name="terms" id="terms" type="checkbox">

    <br>

    <div id="validationMessages"></div>

    <br>

    @isset($user)
        <input type="hidden" id="userId" value="{{$user->id}}">
    @endif

    <input type="submit" value="Save">
</form>
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>

</body>
</html>
