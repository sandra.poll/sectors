-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: proovitoo
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sectors`
--

DROP TABLE IF EXISTS `sectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sectors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_group_name` tinyint(1) NOT NULL,
  `level` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sectors`
--

LOCK TABLES `sectors` WRITE;
/*!40000 ALTER TABLE `sectors` DISABLE KEYS */;
INSERT INTO `sectors` VALUES (1,'Manufacturing',1,1),(2,'Construction materials',0,2),(3,'Electronics and Optics',0,2),(4,'Food and Beverage',1,2),(5,'Bakery & confectionery products',0,3),(6,'Beverages',0,3),(7,'Fish & fish products',0,3),(8,'Meat & meat products',0,3),(9,'Milk & dairy products',0,3),(10,'Other',0,3),(11,'Sweets & snack food',0,3),(12,'Furniture',1,2),(13,'Bathroom/sauna',0,3),(14,'Bedroom',0,3),(15,'Children’s room',0,3),(16,'Kitchen',0,3),(17,'Living room ',0,3),(18,'Office',0,3),(19,'Other (Furniture)',0,3),(20,'Outdoor',0,3),(21,'Project furniture',0,3),(22,'Machinery',1,2),(23,'Machinery components',0,3),(24,'Machinery equipment/tools',0,3),(25,'Manufacture of machinery',0,3),(26,'Maritime',1,3),(27,'Aluminium and steel workboats',0,4),(28,'Boat/Yacht building',0,4),(29,'Ship repair and conversion',0,4),(30,'Metal structures',0,3),(31,'Other',0,3),(32,'Repair and maintenance service',0,3),(33,'Metalworking',1,2),(34,'Construction of metal structures',0,3),(35,'Houses and buildings',0,3),(36,'Metal products',0,3),(37,'Metal works',1,3),(38,'CNC-machining',0,4),(39,'Forgings, Fasteners',0,4),(40,'Gas, Plasma, Laser cutting',0,4),(41,'MIG, TIG, Aluminum welding',0,4),(42,'Plastic and Rubber',1,2),(43,'Packaging',0,3),(44,'Plastic goods',0,3),(45,'Plastic processing technology',1,3),(46,'Blowing',0,4),(47,'Moulding',0,4),(48,'Plastics welding and processing',0,4),(49,'Plastic profiles',0,3),(50,'Printing',1,2),(51,'Advertising',0,3),(52,'Book/Periodicals printing',0,3),(53,'Labelling and packaging printing',0,3),(54,'Textile and Clothing',1,2),(55,'Clothing',0,3),(56,'Textile',0,3),(57,'Wood',1,2),(58,'Other (Wood)',0,3),(59,'Wooden building materials',0,3),(60,'Wooden houses',0,3),(61,'Other',1,1),(62,'Creative industries',0,2),(63,'Energy technology',0,2),(64,'Environment',0,2),(65,'Service',1,1),(66,'Business services',0,2),(67,'Engineering',0,2),(68,'Information Technology and Telecommunications',1,2),(69,'Data processing, Web portals, E-marketing',0,3),(70,'Programming, Consultancy',0,3),(71,'Software, Hardware',0,3),(72,'Telecommunications',0,3),(73,'Tourism',0,2),(74,'Translation services',0,2),(75,'Transport and Logistics',1,2),(76,'Air',0,3),(77,'Rail',0,3),(78,'Road',0,3),(79,'Water',0,3);
/*!40000 ALTER TABLE `sectors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-23 19:08:33
