<?php

namespace App\Http\Controllers;

use App\Models\Sector;

class SectorController extends Controller
{
    public function index()
    {
        return view('user')->with('sectors', Sector::all());
    }
}
