<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return User::with('sectors')->get();
    }

    public function show($id)
    {
        return User::with('sectors')->find($id);
    }

    public function store(Request $request)
    {
        $request->validate(['name' => 'required', 'sectors' => 'required']);

        $collection = collect($request->input('sectors'));

        $collection->transform(
            function ($item, $key) {
                return (int)$item;
            }
        );

        $user = User::create(['name' => $request->input('name')]);
        $sector = $user->sectors()->sync($collection->all());

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $name = $request->input('name');

        $request->validate(['name' => 'required', 'sectors' => 'required']);

        $user->update(['name' => $name]);

        $collection = collect($request->input('sectors'));

        $collection->transform(
            function ($item, $key) {
                return (int)$item;
            }
        );

        $sector = $user->sectors()->sync($collection->all());

        return $user;
    }

}
