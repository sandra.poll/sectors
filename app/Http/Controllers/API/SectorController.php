<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Sector;

class SectorController extends Controller
{
    public function index()
    {
        return Sector::all();
    }
}
