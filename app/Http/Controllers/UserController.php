<?php

namespace App\Http\Controllers;

use App\Models\Sector;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return view('user')->with(['sectors' => Sector::all()]);
    }

    public function show($id)
    {
        $user = User::with('sectors')->findOrFail($id);
        return view('user')->with(['user' => $user, 'sectors' => Sector::all()]);
    }
}
