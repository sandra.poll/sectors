<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function sectors()
    {
        return $this->belongsToMany('App\Models\Sector');
    }

    public function getSectorIdsAttribute()
    {
        $userSectorIds = [];

        foreach ($this->sectors as $sector) {
            $userSectorIds[] = $sector->id;
        }
        return $userSectorIds;
    }
}
